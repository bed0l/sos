System Obsługi Serwisów - S.O.S.
================================

Projekt ma na celu wytworzenie aplikacji webowej mającej na celu wpomaganie serwosów komputerowych (i nie tylko ?).

Przydatne linki:
[Podstawy obsługi GIT](http://git-scm.com/book/pl/Ga%C5%82%C4%99zie-Gita-Podstawy-rozga%C5%82%C4%99ziania-i-scalania)

***

Po pobraniu projektu:

```ruby
rake db:setup
```

## Login i hasło domyślego administratora:

	login: admin
	password: administrator

***

## MySQL

W celu używania bazy danych MySQL nalerzy postępować następująco:

W Gemfile musimy posiadać wpis:

```ruby
gem 'mysql2'
```
W przypadku występowania błędów podczas instalowania gemów np:

```ruby
Gem::Ext::BuildError: ERROR: Failed to build gem native extension.

    /home/USER/.rvm/rubies/ruby-2.1.0/bin/ruby extconf.rb 
checking for ruby/thread.h... yes
checking for rb_thread_call_without_gvl() in ruby/thread.h... yes
checking for rb_thread_blocking_region()... yes

...

extconf failed, exit code 1

Gem files will remain installed in /home/USER/.rvm/gems/ruby-2.1.0/gems/mysql2-0.3.16 for inspection.
Results logged to /home/USER/.rvm/gems/ruby-2.1.0/extensions/x86_64-linux/2.1.0/mysql2-0.3.16/gem_make.out
An error occurred while installing mysql2 (0.3.16), and Bundler cannot continue.
Make sure that `gem install mysql2 -v '0.3.16'` succeeds before bundling.
```

Należy w UBUNTU zaistalować pakiet:

	sudo apt-get install libmysql-ruby libmysqlclient-dev

Jeżeli powyższa komenda nie zadziała, wpisujemy:

	sudo apt-get install libmysqlclient-dev

W RedHat/CentOS/Fedora itp :

	sudo yum install mysql-devel

W MacOS X:

	brew install mysql

Po zainstalowaniu pakietów dokonujemy instalacji gemów poprzez:

```ruby
bundle install
```

Od tego momentu będziemy mogli skonfigurować bazę danych MySQL w aplikacji.

***

## Konfiguracja bazy danych MySQL:

Konfigurujmy odpowiednio bazę danych MySQL poprzez dodanie danych do bazydanych.

```ruby
# file: config/database.yml 

# MySQL.  Versions 4.1 and 5.0 are recommended.
#
# Install the MYSQL driver
#   gem install mysql2
#
# Ensure the MySQL gem is defined in your Gemfile
#   gem 'mysql2'
#
# And be sure to use new-style password hashing:
#   http://dev.mysql.com/doc/refman/5.0/en/old-client.html
development:
  adapter: mysql2
  encoding: utf8
  database: sos_development
  pool: 5
  username: root
  password:
  socket: /var/run/mysqld/mysqld.sock

# Warning: The database defined as "test" will be erased and
# re-generated from your development database when you run "rake".
# Do not set this db to the same as development or production.
test:
  adapter: mysql2
  encoding: utf8
  database: sos_test
  pool: 5
  username: root
  password:
  socket: /var/run/mysqld/mysqld.sock

production:
  adapter: mysql2
  encoding: utf8
  database: sos_production
  pool: 5
  username: root
  password:
  socket: /var/run/mysqld/mysqld.sock
```

***

Do projektu dodano gem "rails-erd". Pozwala on na generowanie diagramu klas z dostępnych w aplikacji modeli.
W celu wygenerowania diagramu wpisujemy w konsoli (będąc w katalogu z projektem):

```Ruby

rake erd:generate

```

W folderze z projektem pojawi się plik "erd.pdf", zawierający diagram.

***

Generowanie diagramu klas:

```Ruby

rake diagram:all

```

***

Niedługo zostanie utworzona Wiki do projektu w celu opisania rozwiazań niektórych problemów.

Rozwiązanie problemu z Mysql:

[link](http://stackoverflow.com/questions/21772116/error-while-install-mysql-gem)
<http://stackoverflow.com/questions/21772116/error-while-install-mysql-gem>

<http://stackoverflow.com/questions/3608287/error-installing-mysql2-failed-to-build-gem-native-extension?lq=1>

Bug z flash messages:

<http://stackoverflow.com/questions/17931534/how-to-define-flash-notifications-with-twitter-bootstrap-rails-gem>