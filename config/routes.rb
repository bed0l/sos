Sos::Application.routes.draw do
  devise_for :users, :controllers => {:registrations => "registrations"}

  # Strona startowa
  devise_scope :user do
    authenticated :user do # jeśli zalogowany
     root :to => 'tasks#index', as: :authenticated_root
   end
   unauthenticated :user do #jeśli nie zalogowany
     root :to => 'devise/sessions#new', as: :unauthenticated_root
   end
  end

  # Linki do panelu administracyjnego
  namespace :admin do
    # get "home/index" # Strona główna panelu adminisracyjnego
    get "dashboard" => 'home#index', as: :dashboard # Strona główna panelu adminisracyjnego
    resources :users
    get 'users/:id/gen_password' => 'users#gen_new_pass', as: :new_password_gen
    resources :roles, only: [:index, :edit, :update]
    get "logs" => 'logs#list', as: :logs #Logi
  end

  resources :tasks do
   resources :items

   # Odepnij sprzęt od zlecenia
   get 'items/:id/remove_from_task' => 'items#remove_from_task', as: :remove_item

   # Przypnij sprzęt do zlecenia
   get 'items/:id/add_to_task' => 'items#add_to_task', as: :add_item

   # Lista klientów do możliwych do dodania do zgłoszenia
   get '/:id/clients' => 'users#clients_list_to_task', as: :clients, on: :collection
   get '/:task_id/client/new' => 'users#new_client_to_task', as: :new_client, on: :collection
   get '/:task_id/clients/:id/add_to_task' => 'users#add_to_task', as: :add_client, on: :collection
   get '/:task_id/clients/:id/remove_from_task' => 'users#remove_from_task', as: :remove_client, on: :collection
   post '/clients' => 'users#create_task_client'
  end

  get 'archive' => 'tasks#archive', as: :archive

  resources :users

  # linki do klientów, zwenętrznej
  get 'clients' => 'users#clients', as: :clients_list
  get 'client/new' => 'users#new_client', as: :client_new
  post 'users/create_client'
  get 'client/:id' => 'users#show_client', as: :show_client
  get 'client/:id/edit' => 'users#edit', as: :edit_client
  delete 'client/:id' => 'users#destroy', as: :del_client
  put 'client/:id' => 'users#update'
  patch 'client/:id' => 'users#update' 
  get 'clients/:id/new/password' => 'users#pass_restart', as: :password_gen

  # linki do sprzętów, zwenętrznej
  get 'items' => 'items#items_list', as: :items_list
  get 'item/new' => 'items#item_new', as: :item_new
  post 'items/item_create'
  get 'item/:id' => 'items#item_show', as: :show_item
  get 'item/:id/edit' => 'items#item_edit', as: :edit_item
  delete 'item/delete/:id' => 'items#item_destroy', as: :del_item
  put 'item/:id/update' => 'items#item_update'
  patch 'item/:id/update' => 'items#item_update' 

  # Linki zarządzające profilem użytkownika
  get 'profile/:id' => 'users#profile', as: :profile
  get 'profile/:id/edit' => 'users#edit_profile', as: :profile_edit
  get 'profile/:id/password' => 'users#change_password', as: :profile_password
  put 'profile/:id' => 'users#profile_update'  # update profilu
  patch 'profile/:id' => 'users#profile_update' # update profilu
  put 'profile/:id/password' => 'users#password_update' # zmiana hasła
  patch 'profile/:id/password' => 'users#password_update' # zmiana hasła

end