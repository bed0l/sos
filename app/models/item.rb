class Item < ActiveRecord::Base
  has_many :cards
  has_many :tasks, :through => :cards

  validates  :item_type, presence: true
  validates  :item_producer, presence: true
  validates  :item_model, presence: true
end
