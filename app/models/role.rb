class Role < ActiveRecord::Base
	has_many :users
	validates  :long_name, presence: true
end
