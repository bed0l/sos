class User < ActiveRecord::Base
  has_one :tasks
  belongs_to :role

  attr_accessor :login
  after_create :set_default_role
  #after_create :generate_username
  # after_create :set_first_user_as_admin

  # Include default devise modules. Others available are:
  # :confirmable, :lockable and :omniauthable  :registerable, 
  devise :database_authenticatable, :rememberable, :recoverable, :trackable, :validatable, :timeoutable


  validates :username, :uniqueness => { :case_sensitive => false }
  validates  :full_name, presence: true

    def self.find_first_by_auth_conditions(warden_conditions)
      conditions = warden_conditions.dup
      if login = conditions.delete(:login)
        where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
      else
        where(conditions).first
      end
    end

    # generowanie nazwy uzytkonika
    def generate_username
      name_lowcase = self.full_name.downcase #Wszystkie literki zmień na małe
      name_ws = name_lowcase.gsub(/\s+/, "") #usuwanie spacji
      number = User.last.id #Pobierz id ostatniego użytkownika
      number += 1 #incnrement id
      name = "#{name_ws}#{number}"
      return name
    end

private
  # Ustaw domyślną rolę dla nowo zarejestrowanego użytkownika.
  def set_default_role
    user_to_role = User.find(self.id) # Znajdź nowego użytkownika zaraz po utworzeniu użytkownika
    if user_to_role.id != 1
      user_to_role.role_id = 4 # Przypisz mu aktomatycznie rolę klienta
      user_to_role.save
    else
      user_to_role.role_id = 1 # Przypisz mu aktomatycznie rolę klienta
      user_to_role.save
    end
  end

end