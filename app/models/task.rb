class Task < ActiveRecord::Base
  # asocjacje
  has_many :cards
  has_many :items, :through => :cards

  belongs_to :status
  belongs_to :type

  belongs_to :user
  # walidacje pól
  validates  :description, presence: true
  validates  :description, length: { minimum: 3, too_short: "Minimalna ilość znaków: %{count}.", maksimum: 300, too_long: "Maksymalna ilość znaków: %{count}."}
end
