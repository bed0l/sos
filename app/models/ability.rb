class Ability
  include CanCan::Ability

  def initialize(user)

       alias_action :create, :read, :update, :destroy, :to => :crud
      # alias_action :create, :read, :update, :to => :cru
      alias_action :create, :update, :destroy, :to => :cud

       user ||= User.new  # Gość - osoba nie zalogowanai
	       cannot :read, :all

          # Administrator systemu
       if user.role.id == 1
         can :manage, :all

          # Kierownik serwisu
        elsif user.role.id == 2
          can [:read, :update, :create], [Task, Item, User]
          cannot :destroy, [Task, Item, User]
          can [:read, :create, :update], [:task, :profile, :clients, :edit, :item, :pass]

          # Pracownik serwisu
        elsif user.role.id == 3
          can [:read, :update, :create], [Task, Item, User]
          cannot :destroy, [Task, Item, User]
          can [:read, :create, :update], [:task, :profile, :clients, :edit, :item, :pass]

          # Klient serwisu
         elsif user.role.id == 4
          can :read, [Task, :show_item]
          cannot [:create, :update, :destroy], Task
          can [:read, :update], [:task, :profile, :edit]

          cannot :read, [Admin,Item]

       else 
         cannot :read, :all
        
      end

  end
end
