module ApplicationHelper

	def full_tiltle(page_title)
		base = "S.O.S."
			if page_title.empty?
				return "#{base}"
			else
				return "#{base} - #{page_title}"
			end
	end

	def flash_class(level)
	    case level
	    when :notice then "alert alert-info"
	    when :success then "alert alert-success"
	    when :error then "alert alert-danger"
	    when :alert then "alert alert-danger"
	    end
	end
end
