module TaskHelper

# helper kolorujący wiersz w tabeli w zależności od terminu zgłoszenia
	def timesup_class(end_date)
		if end_date <= 1.day.from_now
			return ("class = \"danger text-danger\"").html_safe
		elsif end_date <= 7.day.from_now
			return ("class = \"warning text-warning\"").html_safe
		end

	end

# helper dodający label przy terminie końca zlecenia
	def timesup_label(end_date)
		if end_date <= 1.days.from_now
			return ("<span class=\"label label-danger\">Opóźnienie !</span>").html_safe
		elsif end_date <= 7.days.from_now
			return ("<span class=\"label label-warning\">Termin !</span>").html_safe
		end
	end

# heler do wyświetlania alertów wyświetlanych w widoku szczegółów zgłoszenia
	def timesup_showpage(end_date)
		if end_date <= 1.day.from_now
			return ("<div class=\"alert alert-danger\" role=\"alert\"><strong>UWAGA!</strong> Zgłoszenie jest już opóźnione! Proszę o natychmiastową reakcję!</div>").html_safe
		elsif end_date <= 7.day.from_now
			return ("<div class=\"alert alert-warning\" role=\"alert\"><strong>UWAGA!</strong> Zbliża się termin końcowy zgłoszenia!</div>").html_safe
		end
	end

# wyświetl alert przypominający o terminach i opóźnieniach
	def timesup_alert_danger
		if @tasks.where( 'end_task <= ?', 1.days.from_now).count == 1
			return ("<div class=\"alert alert-danger\" role=\"alert\"><strong>UWAGA!</strong> Masz <strong>#{@tasks.where( 'end_task <= ?', 1.days.from_now).count}</strong>-no zgłoszenie które jest już opóźnione!</div>").html_safe
		elsif @tasks.where( 'end_task <= ?', 1.days.from_now).count > 1
			return ("<div class=\"alert alert-danger\" role=\"alert\"><strong>UWAGA!</strong> Masz <strong>#{@tasks.where( 'end_task <= ?', 1.days.from_now).count}</strong> zgłoszenia które są już opóźnione!</div>").html_safe
		end
	end

 # Zmienić nazwe helpera, podaje liczbę zgłodzeń z terminem oraz opóźnieniem (Należy odjąć liczbę opóźnionych !)
	def timesup_alert_warning
		if (@tasks.where( 'end_task <= ?', 7.days.from_now).count)-(@tasks.where( 'end_task <= ?', 1.days.from_now).count) != 0
			# Jeśli jest tylko jedno takie zlecenie
			if (@tasks.where( 'end_task <= ?', 7.days.from_now).count)-(@tasks.where( 'end_task <= ?', 1.days.from_now).count) == 1 
				return ("<div class=\"alert alert-warning\" role=\"alert\"><strong>UWAGA!</strong> Masz <strong>#{(@tasks.where( 'end_task <= ?', 7.days.from_now).count)-(@tasks.where( 'end_task <= ?', 1.days.from_now).count)}</strong>-no zgłoszenie któremu wkrótce skończy się termin realizacji!</div>").html_safe
			# Jeśli do końca zgłoszenia pozostał tydzień
			elsif (@tasks.where( 'end_task <= ?', 7.days.from_now).count)-(@tasks.where( 'end_task <= ?', 1.days.from_now).count) > 0 
				return ("<div class=\"alert alert-warning\" role=\"alert\"><strong>UWAGA!</strong> Masz <strong>#{(@tasks.where( 'end_task <= ?', 7.days.from_now).count)-(@tasks.where( 'end_task <= ?', 1.days.from_now).count)}</strong> zgłoszenia którym wkrótce skończy się termin realizacji!</div>").html_safe
			end
		end 
	end

	def did_client_fly_with_us(fly)
		if fly == nil
			return ("<div class=\"alert alert-danger\" role=\"alert\"><strong>UWAGA!</strong> Do zgłoszenia nie masz dodane aktualnie żadnego klienta! Dodaj go czym prędzej !</div>").html_safe
		end
	end

	def you_have_my_sword(sword)
		if sword.empty?
			return ("<div class=\"alert alert-warning\" role=\"alert\"><strong>UWAGA!</strong> Do zgłoszenia nie masz przypisane aktualnie żadnego sprzętu! Proszę dodaj sprzęt do serwisowania czym prędzej!</div>").html_safe
		end
	end
end
