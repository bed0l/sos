class Admin::HomeController < AdminController
	before_filter :authenticate_user!
	# skip_authorize_resource
  def index
  	@tasks = Task.last(5)
  	@users = User.last(5)
  	@items = Item.last(5)
  	@logs = Log.last(5)

  	authorize! :read, :home
  end
end