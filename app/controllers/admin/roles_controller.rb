class Admin::RolesController < AdminController
	before_filter :authenticate_user!
	before_action :find_role, only: [:edit, :update]
	authorize_resource
  def index
    @roles = Role.all
  end

  def edit
  end 

  def update
    @role.update(role_params)
    if @role.save
      redirect_to admin_roles_path, notice: "Pomyślnie zaktualizowano nazwę roli."
       new_log("Zaktualizowano nazwę w roli: #{@client.name} na #{@client.long_name}.", "Użytkownicy")
    end
  end

  private

    def role_params
      params.require(:role).permit(:long_name)
    end

    def find_role
    	@role = Role.find(params[:id])
    end
end