class Admin::LogsController < AdminController
	before_filter :authenticate_user!

  def list
 	@logs = Log.paginate(:page => params[:page], :per_page => 10).order(created_at: :desc)
  	authorize! :read, :log
  end
end