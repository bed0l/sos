class Admin::UsersController < AdminController

	before_action :find_user, only: [:show, :edit, :update, :destroy]
	authorize_resource :except => [:gen_new_pass]
  def index
    # admin/users
    @users = User.paginate(:page => params[:page], :per_page => 10).order(created_at: :asc)
  end

  def show
  end

  def edit 
  end

  def update
  	@user.update(user_params)
  		if @user.save
  			redirect_to admin_user_path, notice: "Użytkownik został prawidłowo zaktualizowany!"
        new_log("Zaktualizowano profil użytkownika o id: #{@user.id}, loginie #{@user.username}.", "Użytkownicy")
      else
        render "edit"
      end
  end

  def new
    @user = User.new
  end

  def create
      @user = User.new(user_params)
      @user.username = @user.generate_username
      generated_password = Devise.friendly_token.first(8)
      @user.password = generated_password
      if @user.save
        UserMailer.welcome(@user, generated_password).deliver
        redirect_to admin_users_path, notice: "Utworzono nowego prawidłowo użytkownika!"
        new_log("Utworzono nowego użytkownika o id: #{@user.id}, loginie #{@user.username}.", "Użytkownicy")
      else
        render "new"
      end
  end

  def gen_new_pass
    # generowanie nowego hasła
    @user = User.find(params[:id])
    generated_password = Devise.friendly_token.first(8)
    @user.password = generated_password

    if @user.save
        UserMailer.new_password(@user, generated_password).deliver
        redirect_to admin_users_path, notice: "Zrestartowano hasło użytkownika!"
        new_log("Zrestartowano hasło użytkownika o id: #{@user.id}, loginie #{@user.username}.", "Użytkownicy")
      else
        render "index", alert: "Nie udało się zrestartować hasła użytkownikowi"
      end
  end

  def destroy
    UserMailer.destroyed(@user).deliver
    new_log("Usunięto użytkownika z bazy danych o id: #{@user.id}.", "Użytkownicy")
    @user.destroy
    redirect_to admin_users_path, notice: "Użytkownik został poprawnie usunięty."
  end

  private

    def user_params
    	params.require(:user).permit(:user_id, :password, :username, :email, :full_name, :street, :city, :zip_code, :telephone, :nip, :note, :role_id)
    end

    def find_user
    	@user = User.find(params[:id])
    end
end