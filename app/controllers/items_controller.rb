class ItemsController < ApplicationController
  before_filter :authenticate_user! 
  before_action :find_item, only: [:edit, :update, :destroy, :show]
  authorize_resource :except => [:remove_from_task, :add_to_task, :add_or_create, :show, :items_list, :item_new, :item_edit, :item_create, :item_destroy, :item_update, :item_show]

  def index
    @can_add = true
    @task = Task.find(params[:task_id])
  	@items = Item.paginate(:page => params[:page], :per_page => 10).order(created_at: :desc)
  end

  def items_list
    @items = Item.paginate(:page => params[:page], :per_page => 10).order(created_at: :desc)
    authorize! :read, :item
  end

  def item_new
    @item = Item.new
    authorize! :create, :item
  end

  def item_show
    @item = Item.find(params[:id])
    authorize! :read, :item
  end

  def item_edit
    @item = Item.find(params[:id])
    authorize! :update, :item
  end

  def item_create
    @item = Item.new(item_params)
    if @item.save
      redirect_to show_item_path(@item), notice: "Karta sprzętu została poprawnie utworzona!"
      new_log("Utworzono nową kartę sprzętu o numerze: #{@item.id} - [#{@item.item_producer}] [#{@item.item_model}]", "Karty sprzętu")
    else
      render "item_new"
    end
    authorize! :create, :item
  end

  def item_update
    @item = Item.find(params[:id])
    @item.update(item_params)
    if @item.save
      redirect_to items_list_path, notice: "Kartas sprzętu została zaktualizowana!"
      new_log("Zaktualizowano kartę sprzętu o id: #{@item.id} - [#{@item.item_producer}] [#{@item.item_model}]", "Karty sprzętu")
    end
    authorize! :update, :item
  end

  def item_destroy
    @item = Item.find(params[:id])
    new_log("Usunięto kartę sprzętu o numerze: #{@item.id} - [#{@item.item_producer}] [#{@item.item_model}]", "Karty sprzętu")
    @item.destroy
    if @item.save
      redirect_to items_list_path, notice: "Karta sprzętu została porawnie usunięta!"
    end
    authorize! :destroy, :item
  end

  def show
    @task = Task.find(params[:task_id])
    authorize! :read, :show_item
  end

  def new
    @task = Task.find(params[:task_id])
    @item = @task.items.build
  end

  def edit
    @task = Task.find(params[:task_id])
  end

  def create
    @task = Task.find(params[:task_id])
  	@item = @task.items.build(item_params)
  	if @item.save
      @task.items << @item
      # Jeśli będę chciał przekierować na Items#index to ścieżka: task_items_path
  		redirect_to task_items_path, notice: "Karta sprzętu o numerze: #{@item.id} została utworzona, oraz została poprawnie przypięta do zgłoszenia serwisowego o numerze: #{@task.id}!"
      new_log("Utworzono nową kartę sprzętu oraz przypisano ją do zgłoszenia", "Karty sprzętu")
  	else
  		render "new"
  	end
  end

  def destroy
  	@item = Item.find(params[:id])
    new_log("Usunięto kartę sprzętu o numerze: #{@item.id} - [#{@item.item_producer}] [#{@item.item_model}]", "Karty sprzętu")
    @item.destroy
    if @item.save
      redirect_to task_items_path, notice: "Karta sprzętu została porawnie usunięta!"
  	end
  end

  def update
    @task = Task.find(params[:task_id])
  	@item.update(item_params)
  	if @item.save
  		redirect_to task_item_path, notice: "Karta sprzętu została poprawnie zaktualizowana!"
      new_log("Zaktualizowano kartę sprzętu o id: #{@item.id} - [#{@item.item_producer}] [#{@item.item_model}]", "Karty sprzętu")
  	end
  end

  def add_or_create
    @items = Item.paginate(:page => params[:page], :per_page => 10).order(created_at: :desc)
    authorize! :update, :item
  end

  def add_to_task
    @item = Item.find(params[:id])
    @task = Task.find(params[:task_id])
    
    # Wyświetl tę zdefiniowana kartę sprzętu
    @a = @task.items.find_by_id(@item.id)

    # Czy karty sprzętu nie ma dołączonej do zgłoszenia ?
    if @a.nil? 
      @task.items << @item
      new_log("Przypięto sprzęt o numerze: #{@item.id} do zgłoszenia o id: #{@task.id}.", "Sprzęt")
      redirect_to task_items_path(@task), notice: "Przypięto sprzęt o numerze: #{@item.id}, do zgłoszenia."
    else
      redirect_to @task, alert: "Nie można podpiąć tego samego sprzętu do jednego zlecenia!"
    end
    authorize! :update, :item
  end

 # Jeśli jest kilka tych samych kart sprzętowych w zeceniu, wywołanie akcji usunie je wszystkie !
  def remove_from_task
    @task = Task.find(params[:task_id])
    @item = Item.find(params[:id])
    
     if @task.items.delete(@item)
      new_log("Odepnięto sprzęt o numerze: #{@item.id} od zgłoszenia o id: #{@task.id}.", "Sprzęt")
      redirect_to task_items_path(@task), notice: "Odpięto sprzęt o numerze: #{@item.id} od zgłoszenia."
    end
    authorize! :update, :item
  end

  private

  def item_params
  	params.require(:item).permit(:item_type, :item_producer, :item_model, :serial_number, :purchase_date, :note, :description, :task_id, :used)
  end

  def find_item
    @item = Item.find(params[:id])
  end
end
