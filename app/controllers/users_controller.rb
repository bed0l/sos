class UsersController < ApplicationController
	before_filter :authenticate_user!
  authorize_resource :except => [:profile, :edit_profile, :profile_update, :change_password, :clients, :new_client, :show_client, :clients_list_to_task, :create_client, :add_to_task, :remove_from_task, :create_task_client, :new_client_to_task, :password_update, :pass_restart]

  def profile
    # Wyświetlanie profilu użytkownika
    @user = User.find(params[:id])
    authorize! :read, :profile
  end

  def pass_restart
    # resetowanie hasła
    @user = User.find(params[:id])
    generated_password = Devise.friendly_token.first(8)
    @user.password = generated_password

    if @user.save
        UserMailer.new_password(@user, generated_password).deliver
        redirect_to authenticated_root_path, notice: "Zrestartowano hasło użytkownika!"
        new_log("Zrestartowano hasło użytkownika o id: #{@user.id}, loginie #{@user.username}.", "Klientci")
      else
        render "clients", alert: "Nie udało się zrestartować hasła użytkownikowi"
      end
      authorize! :update, :pass
  end

  def profile_update
    @client = User.find(params[:id])
    @client.update(client_params)
    respond_to do |format|
      if @client.save
          format.html { redirect_to profile_path, notice: "Twoje dane zostały poprawnie zaktualizowane." }
          format.json { render action: 'profile', status: :created, location: @client }
          new_log("Klient o loginie: #{@client.username}, zaktualizował swój profil.", "Klienci - Profil")
      else
        format.html { render action: 'edit_profile' }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
    authorize! :update, :profile
  end

  def password_update
    @client = User.find(params[:id])
    @client.update(client_password)
    respond_to do |format|
      if @client.save
          format.html { redirect_to profile_path, notice: "Twoje hasło zostało zmienione." }
          format.json { render action: 'profile', status: :created, location: @client }
          new_log("Klient o loginie: #{@client.username}, zmienił swoje hasło.", "Klienci - Profil")
      else
        format.html { render action: 'change_password' }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
  end
    authorize! :update, :profile
  end

  def edit_profile
    @client = User.find(params[:id])
    authorize! :update, :edit
  end

  def change_password
    @client = User.find(params[:id])
    authorize! :update, :edit
  end

  def clients
    # Lista użytkowników z rolą klient
    @clients = User.paginate(:page => params[:page], :per_page => 10).where('role_id = ?', 4)
    authorize! :read, :clients
  end

  def clients_list_to_task
    @task = Task.find(params[:id])
    @clients = User.paginate(:page => params[:page], :per_page => 10).where('role_id = ?', 4)
    authorize! :update, :clients
  end

  def new_client
    @client = User.new
    authorize! :create, :clients
  end

  def new_client_to_task
    @task = Task.find(params[:task_id])
    @client = User.new
    authorize! :create, :clients
  end

  def show_client
    @client = User.find(params[:id])
    authorize! :read, :clients
  end

  def create_client
    @client = User.new(user_params)
    @client.username = @client.generate_username
    generated_password = Devise.friendly_token.first(8)
    @client.password = generated_password
    new_log("Utworzono nowego klienta o id: #{@client.id}, loginie: #{@client.username}", "Klienci")

    if @client.save
      UserMailer.welcome(@client, generated_password).deliver
      redirect_to clients_list_path, notice: "Utworzono prawidłowo nowego klienta o id: #{@client.id}!"
    else
      render "new_client"
    end
    authorize! :create, :clients
  end

  def create_task_client
    @task = Task.find(params[:task_id])
    @client = User.new(user_params)
    @client.username = @client.generate_username
    generated_password = Devise.friendly_token.first(8)
    @client.password = generated_password

    if @client.save
        @task.user_id = @client.id
        @task.save
        UserMailer.welcome(@client, generated_password).deliver
        redirect_to task_items_path(@task), notice: "Utworzono prawidłowo klienta o id: #{@client.id}, oraz przypisano go do zgłoszenia o id; #{@task.id}."
        new_log("Utworzono nowego klienta o id: #{@client.id}, loginie #{@client.username}, oraz przypisano go do zgłoszenia o id: #{@task.id}", "Klienci")
      else
        render "new_client_to_task"
      end
    authorize! :create, :clients
  end

  def edit
    @client = User.find(params[:id])
  end

  def update
    @client = User.find(params[:id])
    @client.update(user_params)
    respond_to do |format|
      if @client.save
          format.html { redirect_to show_client_path(@client), notice: "Dane klienta zostały poprawnie zaktualizowane!" }
          format.json { render action: 'clients', status: :created, location: @client }
          new_log("Zaktializowano dane klientna: #{@client.id}, loginie: #{@client.username}", "Klienci")
      else
        format.html { render action: 'edit' }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @client = User.find(params[:id])
    new_log("Usunięto z systemu klienta o id: #{@client.id}", "Użytkownicy")
    @client.destroy
    respond_to do |format|
  
    UserMailer.destroyed(@client).deliver
    format.html { redirect_to clients_list_path, notice: "Klient został poprawnie usunięty z serwisu." }
    format.json { render action: 'clients', status: :created, location: @client }
  end
  end

  def add_to_task
    @client = User.find(params[:id])
    @task = Task.find(params[:task_id])

    if @task.user.nil?
      @task.user_id = @client.id
      @task.save
      new_log("Przypięto klienta (id: #{@client.id}) do zgłoszenia (id: #{@task.id})", "Klienci")
      redirect_to task_items_path(@task), notice: "Przypięto klienta o numerze: #{@client.id}, do zgłoszenia o id #{@task.id}."
    else
      redirect_to @task, alert: "Nie można już podpiąć kolejnego klienta do tego samego zgłoszenia!"
    end
    authorize! :update, :clients
  end

  def remove_from_task
    @client = User.find(params[:id])
    @task = Task.find(params[:task_id])
    @task.user_id = nil
     if @task.user_id.nil?
      new_log("Odepnięto klienta od zgłoszenia id: #{@task.id} ", "Klientci")
      @task.save
      redirect_to @task, notice: "Odpięto klienta #{@client.username} od tego zgłoszenia."
    end
    authorize! :update, :clients
  end

    def create
      @user = User.new(user_params)
      @user.username = @user.generate_username
      generated_password = Devise.friendly_token.first(8)
      @user.password = generated_password
      if @user.save
        UserMailer.welcome(@user, generated_password).deliver
        redirect_to admin_users_path, notice: "Utworzono nowego prawidłowo użytkownika!"
        new_log("Utworzono nowego użytkownika o id: #{@user.id}, loginie #{@user.username}.", "Użytkownicy")
      else
        render "new"
      end
  end

private

  def user_params
    params.require(:user).permit(:user_id, :password, :username, :email, :full_name, :street, :city, :zip_code, :telephone, :nip, :note, :role_id)
  end

  def client_params
    params.require(:user).permit(:user_id, :email, :full_name, :street, :city, :zip_code, :telephone, :nip)
  end

  def client_password
    params.require(:user).permit(:password)
  end
end
