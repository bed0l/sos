class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :configure_permitted_parameters, if: :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to authenticated_root_path, :alert => exception.message
  end

  def new_log(info, controller_name)
    @log = Log.new
    @log.note = info
    @log.user_id = current_user.id
    @log.user_name = current_user.username
    @log.ip_address = request.env['REMOTE_ADDR']
    @log.controller = controller_name
    @log.action = action_name
    @log.save
  end
  
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation, :remember_me) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :username, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:email, :password, :password_confirmation, :current_password,:full_name, :street, :city, :zip_code, :nip, :telephone) }
  end
  
end
