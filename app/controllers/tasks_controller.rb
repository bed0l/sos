class TasksController < ApplicationController
  before_filter :authenticate_user!
  before_action :find_task, only: [:show, :edit, :destroy, :update]
  authorize_resource  :except => :archive

  def index
    if current_user.role.name == 'client'
      @tasks = Task.paginate(:page => params[:page], :per_page => 10).order(created_at: :desc).where('archive = ?', false).where('user_id = ?', current_user.id)
    elsif current_user.role.name ==  'employe'
      @tasks = Task.paginate(:page => params[:page], :per_page => 10).order(created_at: :desc).where('archive = ?', false).where('responsible = ?', current_user.full_name)
    else
	     @tasks = Task.paginate(:page => params[:page], :per_page => 10).order(created_at: :desc).where('archive = ?', false)
    end
  end

  def archive
    if current_user.role.name == 'client'
      @tasks = Task.paginate(:page => params[:page], :per_page => 10).where('user_id = ?', current_user.id ).where('archive = ?', true)
    else
      @tasks = Task.paginate(:page => params[:page], :per_page => 10).where('archive = ?', true)
    end
      authorize! :read, :task
  end

  def show

  end

  def edit
  end

  def new
	 @task = Task.new
  end

  def create
      if current_user.role.name == 'menager'
        @task = Task.new(params_for_menager)
      elsif current_user.role.name == 'admin'
        @task = Task.new(params_for_menager)
      else
        @task = Task.new(task_params_for_employee)
      end
	 # @task = Task.new(task_params)
   @task.author = current_user.full_name # imie i nazwisko osoby tworzącej zlecenie
   @task.archive = false
    respond_to do |format|
      if @task.save
        format.html { redirect_to clients_tasks_path(@task), notice: 'Gratulacje! Zgłoszenie serwisowe zostało poprawnie utworzone.' }
        format.json { render action: 'index', status: :created, location: @task }
        new_log("Utworzono nowe zgłoszenie serwisowe o id: #{@task.id}", "Zgłoszenie")
      else
        format.html { render action: 'new' }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    if @task.lock != true
      if current_user.role.name == 'menager'
        @task.update(params_for_menager)
      elsif current_user.role.name == 'admin'
        @task.update(params_for_menager)
      else
  	    @task.update(task_params_for_employee)
      end

      if @task.status_id == 7
         @task.archive = true
         @task.lock = true
         new_log("Zgłoszenie o id: #{@task.id}, zostało zakończone i przeniesione do archiwum.", "Zgłoszenie")
         TaskMailer.closed(@task.user, @task).deliver
        elsif @task.status_id  == 6
          @task.archive = true
          new_log("Zgłoszenie o id: #{@task.id}, zostało anulowane i przeniesione do archiwum.", "Zgłoszenie")
        else
          @task.archive = false
          @task.lock = false
      end

      respond_to do |format|
        if @task.save

          new_log("Zaktualizowano zgłoszenie serwisowe o id: #{@task.id}", "Zgłoszenie")
          format.html { redirect_to tasks_path, notice: 'Zgłoszenie serwisowe zostało poprawnie zaktualizowane.' }
          format.json { render action: 'index', status: :created, location: @task }
        else
          format.html { render action: 'show' }
          format.json { render json: @task.errors, status: :unprocessable_entity }
        end
      end
     else
        redirect_to @task, alert: "Mnie można edytować zakończonego zgłoszenia serwisowego!"
     end
  end

  def destroy
  new_log("Usunięto zgłoszenie serwisowe o id: #{@task.id}", "Zgłoszenie")
   @task.destroy
    respond_to do |format|
      if @task.save
        format.html { redirect_to tasks_path, notice: 'Zlecenie zostało usunięte.' }
        format.json { render action: 'index', status: :created, location: @task }
      else
        format.html { render action: 'new' }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end



private
# Dopisać parametry do zapisów
  def task_params
	 params.require(:task).permit(:description, :type_id, :status_id, :end_task, :note, :author_id, :archive, :lock)
  end

  def task_params_for_employee
   params.require(:task).permit(:description, :type_id, :status_id, :end_task, :note, :author_id, :archive, :lock)
  end

  def params_for_menager
   params.require(:task).permit(:description, :type_id, :status_id, :end_task, :note, :author_id, :archive, :lock, :responsible)
  end

# Znajdź zgłoszenia po id
  def find_task
	 @task = Task.find(params[:id])
  end
end
