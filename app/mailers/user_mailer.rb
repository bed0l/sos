class UserMailer < ActionMailer::Base
  default from: "softwaresoscompany@gmail.com" # adres email specjalnie przygotowany do testowania

  # mail po utworzeniu użytkownika
  def welcome(user, gen_password)
  	@user = user
  	@password = gen_password
  	@url = "http://79.189.173.58/"
  	mail(to: @user.email, subject: 'Witaj w Systemie Obsługi Serwisów!')
  end

  def new_password(user, new_password)
    @user = user
    @password = new_password
    @url = "http://79.189.173.58/"
    mail(to: @user.email, subject: 'Twoje hasło zostało zrestartowane!')
  end

  # mail po usunięciu użytkownika
  def destroyed(user)
  	@user = user
  	mail(to: @user.email, subject: 'Usuniecie konta z Systemu Obsługi Serwisów.')
  end

end
