# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140912100739) do

  create_table "cards", force: true do |t|
    t.integer  "task_id"
    t.integer  "item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "items", force: true do |t|
    t.string   "item_type"
    t.string   "item_producer"
    t.string   "item_model"
    t.string   "serial_number"
    t.date     "purchase_date"
    t.text     "note"
    t.text     "description"
    t.integer  "task_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "used"
  end

  add_index "items", ["task_id"], name: "index_items_on_task_id", using: :btree

  create_table "logs", force: true do |t|
    t.string   "user_id"
    t.string   "user_name"
    t.string   "ip_address"
    t.string   "controller"
    t.string   "action"
    t.string   "note"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", force: true do |t|
    t.string "name"
    t.string "long_name"
  end

  create_table "statuses", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tasks", force: true do |t|
    t.integer  "user_id"
    t.integer  "status_id"
    t.integer  "type_id"
    t.text     "description"
    t.text     "note"
    t.date     "end_task"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "author"
    t.boolean  "archive",     default: false
    t.text     "expertise"
    t.string   "cost"
    t.boolean  "lock",        default: false
    t.string   "responsible"
  end

  add_index "tasks", ["status_id"], name: "index_tasks_on_status_id", using: :btree
  add_index "tasks", ["type_id"], name: "index_tasks_on_type_id", using: :btree
  add_index "tasks", ["user_id"], name: "index_tasks_on_user_id", using: :btree

  create_table "types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "username",               default: "", null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "name"
    t.string   "full_name"
    t.string   "street"
    t.string   "city"
    t.string   "zip_code"
    t.integer  "nip"
    t.integer  "telephone"
    t.string   "communicator_type"
    t.string   "communicator"
    t.text     "note"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["role_id"], name: "index_users_on_role_id", using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

end
