class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :item_type
      t.string :item_producer
      t.string :item_model
      t.string :serial_number
      t.date :purchase_date
      t.text :note
      t.text :description
      t.references :task, index: true

      t.timestamps
    end
  end
end
