class AddUsedToItem < ActiveRecord::Migration
  def change
    add_column :items, :used, :boolean
  end
end
