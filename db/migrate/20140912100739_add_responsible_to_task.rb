class AddResponsibleToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :responsible, :string
  end
end
