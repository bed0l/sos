class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.references :user, index: true
      t.references :status, index: true
      t.references :type, index: true
      t.text :description
      t.text :note
      t.date :end_task

      t.timestamps
    end
  end
end
