class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :user_id
      t.string :user_name
      t.string :ip_address
      t.string :controller
      t.string :action
      t.string :note

      t.timestamps
    end
  end
end
