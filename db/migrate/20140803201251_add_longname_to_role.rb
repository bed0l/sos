class AddLongnameToRole < ActiveRecord::Migration
  def change
    add_column :roles, :long_name, :string
  end
end
