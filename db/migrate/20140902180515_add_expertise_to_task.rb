class AddExpertiseToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :expertise, :text
    add_column :tasks, :cost, :string
  end
end
