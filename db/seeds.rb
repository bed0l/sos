# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Status.create([
	{name: 'Przyjęte'},
	{name: 'W naprawie'},
	{name: 'Oczekawianie na decyzje klietna'},
	{name: 'Do wydania'},
	{name: 'Wysłano'},
	{name: 'Anulowane'},
	{name: 'Zakończone'}
	])

Type.create([
	{name: 'Gwarancyjne'},
	{name: 'Usługa płatna'},
	{name: 'Warunkowa'},
	{name: 'Priorytetowe'}
	])

User.create(
	username: 'admin',
	full_name: 'Administrator',
	email: 'admin@admin.com',
	password: 'administrator',
	role_id: '1'
	)

Role.create([
	{name: 'admin', long_name: 'Administrator'},
	{name: 'menager', long_name: 'Kierownik'},
	{name: 'employe', long_name: 'Pracownik'},
	{name: 'client', long_name: 'Klient'}
	])